(function($, undefined) {

Drupal.admin = Drupal.admin || {};
Drupal.admin.behaviors = Drupal.admin.behaviors || {};

Drupal.behaviors.adminDialog = {
  attach: function (context, settings) {
    // Contextual links.
    $('.contextual-links', context).once('admin-dialog', function () {
      $(this).find('a').click(Drupal.admin.dialog.click);
    });
    // All links in tables.
    $('table', context).once('admin-dialog', function () {
      $(this).find('tbody a').click(Drupal.admin.dialog.click);
    });
    // Forms within admin dialogs.
    // @todo Consider to move this to the server-side. Any button without #ajax.
    if ($(context).is('form')) {
      // @todo AJAX doesn't work with this == form itself.
      var $dialog = $(context).parent();
      // @todo :button broken in jQuery 1.4.3 ?
      $('input[type=submit]', context).once('admin-dialog', function () {
        $(this).addClass('use-ajax-submit ajax-processed');

        var wrapper = $dialog.attr('id');
        var element_settings = {
          method: 'html',
          wrapper: wrapper,
          submit: {
            admin_dialog: true,
            admin_dialog_id: wrapper,
            js: true
          }
        };

        // AJAX submits specified in this manner automatically submit to the
        // normal form action.
        element_settings.url = $(this.form).attr('action');
        // Form submit button clicks need to tell the form what was clicked so
        // it gets passed in the POST request.
        element_settings.setClick = true;
        // Form buttons use the 'click' event rather than mousedown.
        element_settings.event = 'click';
        // Clicked form buttons look better with the throbber than the progress bar.
        element_settings.progress = { 'type': 'throbber' };

        var base = wrapper; // $(this).attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      });
    }
  }
};

Drupal.admin.behaviors.dialog = function (context, settings, $adminMenu) {
  if (1 || settings.admin_menu.dialog) {
    $adminMenu.find('a').each(function () {
      $(this).click(Drupal.admin.dialog.click);
    });
  }
};

Drupal.admin.dialog = Drupal.admin.dialog || {
  create: function () {
    // @todo Pass href and re-use existing dialog.
    // @todo Inspect potential destination query parameter and re-use another
    //   potentially existing dialog.
    var $dialog = $('<div></div>').dialog({
      autoOpen: false,
      dialogClass: 'admin-dialog',
      // @todo Add window + stacking + displace logic.
      position: [30, 30],
      width: 'auto',
      minWidth: 400,
      // @todo UI dialog doesn't get it right. See click() below.
      height: 'auto',
      maxHeight: window.innerHeight - 30
    });
    return $dialog;
  },

  click: function () {
    var $link = $(this);
    // Generate an HTML ID, if there is none yet.
    if (!$link.attr('id')) {
      $link.attr('id', 'admin-dialog-link-' + $link.attr('href').replace(/[^a-zA-Z0-9_-]/g, '-'));
    }
    var base = $link.attr('id');
    var wrapper = base + '--dialog-content';

    var $dialog = Drupal.admin.dialog.create();
    $dialog.attr('id', wrapper);
    $dialog.css('maxHeight', window.innerHeight);

    var element_settings = {
      url: $link.attr('href'),
      event: 'click.ajax',
      method: 'html',
      wrapper: wrapper,
      submit: {
        admin_dialog: true,
        admin_dialog_id: wrapper,
        js: true
      }
    };
    Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
    $link.triggerHandler('click.ajax');

    $dialog.dialog('open');
    return false;
  }
};

})(jQuery);
